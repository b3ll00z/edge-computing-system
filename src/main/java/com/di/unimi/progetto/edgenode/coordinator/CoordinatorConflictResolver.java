package com.di.unimi.progetto.edgenode.coordinator;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;
import java.util.Collections;
import java.util.List;

public class CoordinatorConflictResolver extends Thread {

  private EdgeNode node;
  private List<EdgeInfo> conflictingCoordinators;

  public CoordinatorConflictResolver(List<EdgeInfo> activeCoordinators) {
    node = EdgeNode.getInstance();
    conflictingCoordinators = activeCoordinators;
  }

  public void run() {
    System.out.println("Conflict found!");
    Collections.sort(conflictingCoordinators);

    for (EdgeInfo conflicting : conflictingCoordinators) {
      if (!node.getId().equals(conflicting.getName())) node.addEdge(conflicting);
    }

    EdgeDataSensorsReader.getInstance().electionLauncher();
  }
}
