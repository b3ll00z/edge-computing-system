package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.Socket;

/* Classe che avvia un thread per presentarsi a un preciso edge node e sapere se è il coordinatore */
public class EdgeHelloSender extends Thread {

  private EdgeNode node;
  private EdgeInfo info;
  private Gson gson;

  public EdgeHelloSender(EdgeInfo edgeInfo) {
    node = EdgeNode.getInstance();
    info = edgeInfo;
    gson = new GsonBuilder().create();
  }

  public void run() {
    /* Preparazione messaggio di ingresso nella rete da spedire al nodo edge */
    EdgeMessage presentationMessage = new EdgeMessage(node.getId(), EdgeMessage.Content.HELLO);
    presentationMessage.from(node.getIp());
    presentationMessage.setPortForEdges(node.getPortForEdges());

    String jsonPresentationMessage = gson.toJson(presentationMessage);

    /* Invio messaggio. In caso di errore per inattività del nodo edge considerato interrompo il thread */
    try {
      /* Creazione canale di comunicazione col nodo edge che ho rilevato dallo stato della città
       * fornitomi dal server al momento della registrazione */
      Socket establishedSocket =
          new Socket(InetAddress.getByName(info.getIP()), info.getPortForEdgeNodes());
      DataOutputStream outToEdge = new DataOutputStream(establishedSocket.getOutputStream());
      outToEdge.writeBytes(jsonPresentationMessage + "\n");
      outToEdge.flush();
      System.out.println("I've sent hello_message to " + info.getName());
    } catch (IOException e) {
      System.out.println(info.getName() + " isn't in the net anymore");
    }
  }
}
