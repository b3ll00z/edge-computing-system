package com.di.unimi.progetto.edgenode.edges_communication;

import java.util.PriorityQueue;
import java.util.Queue;

/* Questa classe viene usata come coda di inserimento di messaggi in arrivo da altri nodi edge. Vi e' previsto un
 *  solo consumatore, che leggendo un messaggio alla volta fa dovute operazioni */
public class EdgeMessageBuffer {

  public static EdgeMessageBuffer instance = null;
  private Queue<EdgeMessage> buffer;

  private EdgeMessageBuffer() {
    buffer = new PriorityQueue<EdgeMessage>();
  }

  public static synchronized EdgeMessageBuffer getInstance() {
    if (instance == null) instance = new EdgeMessageBuffer();

    return instance;
  }

  public synchronized void push(EdgeMessage edgeMessage) {
    buffer.add(edgeMessage);
    notifyAll();
  }

  public synchronized EdgeMessage pull() {

    while (buffer.size() == 0) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    EdgeMessage message = null;

    if (buffer.size() > 0) {
      message = buffer.poll();
    }

    return message;
  }
}
