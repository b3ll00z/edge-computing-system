package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.simulator.Measurement;
import java.util.Calendar;

public class EdgeMessage implements Comparable<EdgeMessage> {

  public enum Content {
    HELLO,
    WELCOME,
    OK,
    COORDINATOR,
    ELECTION,
    MEASUREMENT
  }

  private String sender;
  private Content content;
  private String from;
  private int portForEdges;
  // private int priority;
  private long timestamp;
  private Measurement measurement;

  public EdgeMessage(String s, Content t) {
    sender = s;
    content = t;
    from = null;
    portForEdges = 0;
    measurement = null;
    timestamp = computeTimestamp();
  }

  public void from(String edgeAddress) {
    from = edgeAddress;
  }

  public String getSenderAddress() {
    return from;
  }

  public void setPortForEdges(int portEdges) {
    portForEdges = portEdges;
  }

  public String getSender() {
    return sender;
  }

  public Content getContent() {
    return content;
  }

  public int getPortForEdges() {
    return portForEdges;
  }

  public void setMeasurement(Measurement m) {
    measurement = m;
  }

  public Measurement getMeasurement() {
    return measurement;
  }

  private long computeTimestamp() {
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);
    return System.currentTimeMillis() - c.getTimeInMillis();
  }

  @Override
  public int compareTo(EdgeMessage o) {
    Long t1 = timestamp;
    Long t2 = o.getTimestamp();
    return t1.compareTo(t2);
  }

  public long getTimestamp() {
    return timestamp;
  }

  @Override
  public String toString() {
    return "EdgeMessage{"
        + "sender='"
        + sender
        + '\''
        + ", content="
        + content
        + ", from='"
        + from
        + '\''
        + ", portForEdges="
        + portForEdges
        + ", timestamp="
        + timestamp
        + ", measurement="
        + measurement
        + '}';
  }
}
