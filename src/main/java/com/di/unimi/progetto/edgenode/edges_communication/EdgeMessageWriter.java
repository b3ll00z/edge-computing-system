package com.di.unimi.progetto.edgenode.edges_communication;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

/* Classe per gestire ogni messaggio pervenuto da un nodo e in base al contenuto attuare certi comportamenti */
public class EdgeMessageWriter extends Thread {

  private EdgeMessageBuffer bufferOfMessages;
  private Socket connectedEdgeSocket;
  private BufferedReader inFromEdge;
  private Gson gson;

  public EdgeMessageWriter(Socket workingSocket) {
    try {
      bufferOfMessages = EdgeMessageBuffer.getInstance();
      connectedEdgeSocket = workingSocket;
      inFromEdge = new BufferedReader(new InputStreamReader(connectedEdgeSocket.getInputStream()));
      gson = new GsonBuilder().create();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void run() {

    try {
      /* Leggo messaggio di risposta e lo bufferizzo*/
      String jsonMessageFromClient = inFromEdge.readLine();

      /* Lo converto da json a EdgeMessage e lo metto nel bufferOfMessages condiviso */
      EdgeMessage edgeMessage = gson.fromJson(jsonMessageFromClient, EdgeMessage.class);
      bufferOfMessages.push(edgeMessage);

      connectedEdgeSocket.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
