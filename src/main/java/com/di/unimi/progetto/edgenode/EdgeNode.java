package com.di.unimi.progetto.edgenode;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.beans.Position;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class EdgeNode {

  private static EdgeNode instance = null;

  private String id;
  private String ip;
  private int portForSensors;
  private int portForEdges;
  private Position positionInCity;
  private boolean isCoordinator;
  private String coordinatorName;
  private List<EdgeInfo> edges;
  private List<EdgeInfo> edgesToNotifyOnElection;
  private ServerSocket socketForEdges;
  private ServerSocket socketForSensors;

  private EdgeNode(String name, String ipAddr, int portEdges, int portSensors) {
    id = name;
    ip = ipAddr;
    portForEdges = portEdges;
    portForSensors = portSensors;
    isCoordinator = false;
    coordinatorName = null;
    edges = new ArrayList<>();
    edgesToNotifyOnElection = new ArrayList<>();
    socketForEdges = null;
    socketForSensors = null;
  }

  public static synchronized void createEdgeNode(
      String name, String ipAddr, int portEdges, int portSensors) {
    if (instance == null) instance = new EdgeNode(name, ipAddr, portEdges, portSensors);
  }

  public static synchronized EdgeNode getInstance() {
    return instance;
  }

  public String getId() {
    return id;
  }

  public String getIp() {
    return ip;
  }

  public int getPortForEdges() {
    return portForEdges;
  }

  public int getPortForSensors() {
    return portForSensors;
  }

  public void generatePositionInCity() {
    positionInCity = Position.generateRandomPosition();
  }

  public Position getPositionInCity() {
    return positionInCity;
  }

  public synchronized boolean isCoordinator() {
    return isCoordinator;
  }

  public synchronized void setCoordinatorName(String cName) {

    if (id.equals(cName)) isCoordinator = true;
    else isCoordinator = false;

    coordinatorName = cName;
  }

  public synchronized String getCoordinatorName() {
    return coordinatorName;
  }

  public void addEdge(EdgeInfo info) {
    synchronized (edges) {
      if (edges.contains(info)) return;

      edges.add(info);

      if (id.compareTo(info.getName()) < 0) {
        synchronized (edgesToNotifyOnElection) {
          edgesToNotifyOnElection.add(info);
          Collections.sort(edgesToNotifyOnElection);
        }
      }
    }
  }

  public List<EdgeInfo> getAllEdges() {
    synchronized (edges) {
      return new ArrayList<EdgeInfo>(edges);
    }
  }

  public List<EdgeInfo> getEdgesToNotifyOnElection() {
    synchronized (edgesToNotifyOnElection) {
      return new ArrayList<EdgeInfo>(edgesToNotifyOnElection);
    }
  }

  public void removeEdge(String nameEdgeToRemove) {
    synchronized (edges) {
      EdgeInfo found = null;

      for (EdgeInfo edgeToRemove : edges) {
        if (edgeToRemove.getName().equals(nameEdgeToRemove)) {
          found = edgeToRemove;
          break;
        }
      }

      if (found == null) return;

      edges.remove(found);

      synchronized (edgesToNotifyOnElection) {
        edgesToNotifyOnElection.remove(found);
      }
    }
  }

  public void clearKnowledge() {
    synchronized (edges) {
      edges.clear();
      synchronized (edgesToNotifyOnElection) {
        edgesToNotifyOnElection.clear();
      }
    }
  }

  public void setSocketForEdges(ServerSocket socket) {
    socketForEdges = socket;
  }

  public ServerSocket getSocketForEdges() {
    return socketForEdges;
  }

  public void setSocketForSensors(ServerSocket socket) {
    socketForSensors = socket;
  }

  public ServerSocket getSocketForSensors() {
    return socketForSensors;
  }
}
