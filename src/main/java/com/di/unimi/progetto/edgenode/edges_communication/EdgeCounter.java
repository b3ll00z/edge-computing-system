package com.di.unimi.progetto.edgenode.edges_communication;

public class EdgeCounter {

  private int attendedResponses;

  public EdgeCounter(int initialCount) {
    attendedResponses = initialCount;
  }

  public synchronized void decrement() {
    if (attendedResponses > 0) attendedResponses -= 1;
  }

  public synchronized boolean nothingToAttend() {
    return attendedResponses == 0;
  }
}
