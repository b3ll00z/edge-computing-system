package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import java.util.ArrayList;
import java.util.List;

/* Un thread di questa classe viene lanciato nel momento in cui andando a trasmettere statistiche locali al coordinatore
 * rilevo la sua assenza. Applicando algoritmo di Bully, provvedo a rilevare la lista dei nodi con nome alfabeticamente
 * più grande del mio. Per ognuno invio un messaggio di elezione, se si verifica un errore aprendo la socket verso un
 * nodo provvedo a decrementare il contatore di Ack e a rimuoverlo dalla conoscenza della rete */
public class EdgeElectionStarter extends Thread {

  private EdgeNode node;

  public EdgeElectionStarter() {
    node = EdgeNode.getInstance();
  }

  public void run() {

    List<EdgeInfo> nodesToNotify = node.getEdgesToNotifyOnElection();

    /* Se non ci sono nodi da comunicare mi autoproclamo coordinatore e apro un handler apposito */
    if (nodesToNotify.isEmpty()) {
      System.out.println("Nobody to notify");
      becomeCoordinator();
      return;
    }

    /* Sono nella situazione in cui devo inviare messaggio di elezione
     * Imposto contatore Ack di OK a seguito del messaggio ELECTION */
    EdgeCounter ackCounter = new EdgeCounter(nodesToNotify.size());

    /* Preparo pool di thread per l'invio broadcast di messaggi ELECTION */
    List<EdgeElectionSender> senders = new ArrayList<>();

    for (EdgeInfo nodeToNotify : nodesToNotify)
      senders.add(
          new EdgeElectionSender(
              nodeToNotify.getName(),
              nodeToNotify.getIP(),
              nodeToNotify.getPortForEdgeNodes(),
              ackCounter));

    try {
      /* Avvio i thread */
      for (EdgeElectionSender sender : senders) sender.start();

      /* Attendo la fine di tutti gli Election sender */
      for (EdgeElectionSender sender : senders) sender.join();

      System.out.println("all election message sent");

      /* Se durante l'invio dei messaggi di elezione verso nodi con ID maggiori del mio ci sono stati errori
       * di connessione provvedo a toglierli dalla lista nodesToNotify, però se al termine di questi thread e'
       * vuota vuol dire che non sono stato in grado di inviare messaggi di elezione, perciò divento coordinatore */
      if (ackCounter.nothingToAttend()) {
        System.out.println("I've discovered I need to be the coordinator");
        becomeCoordinator();
      }
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  private void becomeCoordinator() {
    try {
      EdgeBecomeCoordinatorHandler handler = new EdgeBecomeCoordinatorHandler();
      handler.setPriority(MAX_PRIORITY);
      handler.start();
      handler.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
