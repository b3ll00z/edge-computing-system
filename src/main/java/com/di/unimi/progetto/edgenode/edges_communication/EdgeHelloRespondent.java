package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/* In questa classe vengono gestiti i messaggi ricevuti di tipo Hello, in sostanza li inserisco nella mia conoscenza */
public class EdgeHelloRespondent extends Thread {

  private EdgeNode node;
  private EdgeDataSensorsReader dataSensorsReader;
  private String newEdgeName;
  private String newEdgeIP;
  private int newEdgePortForEdges;

  public EdgeHelloRespondent(
      String name, String ip, int senderPortForEdges, EdgeDataSensorsReader reader) {
    node = EdgeNode.getInstance();
    newEdgeName = name;
    newEdgeIP = ip;
    newEdgePortForEdges = senderPortForEdges;
    dataSensorsReader = reader;
  }

  public void run() {

    /* Preparo oggetto sul nuovo nodo entrato, tenendo conto delle tre informazioni sopra */
    EdgeInfo newEdgeEntered = new EdgeInfo();
    newEdgeEntered.setName(newEdgeName);
    newEdgeEntered.setIP(newEdgeIP);
    newEdgeEntered.setPortForEdgeNodes(newEdgePortForEdges);

    EdgeMessage replyToEdge;

    /* Nel caso si verifichi un'elezione attendo la sua terminazione */
    dataSensorsReader.waitElectionTermination();

    /* Preparazione Ack di risposta */
    if (node.isCoordinator())
      replyToEdge = new EdgeMessage(node.getId(), EdgeMessage.Content.COORDINATOR);
    else replyToEdge = new EdgeMessage(node.getId(), EdgeMessage.Content.WELCOME);

    /* Coordinate del nodo edge affinche' il nuovo nodo entrato possa comunicarmi in futuro */
    replyToEdge.from(node.getIp());
    replyToEdge.setPortForEdges(node.getPortForEdges());

    Gson gson = new GsonBuilder().create();
    String jsonReplyToEdge = gson.toJson(replyToEdge);

    /* Invio ack al nuovo nodo, se si verificano errori in apertura di socket o trasmissione del messaggio
     * viene interrotta la connessione */
    try {
      Socket socket = new Socket(newEdgeIP, newEdgePortForEdges);
      DataOutputStream outToNewEdge = new DataOutputStream(socket.getOutputStream());
      outToNewEdge.writeBytes(jsonReplyToEdge + "\n");
      outToNewEdge.flush();

      /* Inserisco il nuovo nodo nella mia conoscenza della rete peer2peer */
      node.addEdge(newEdgeEntered);
      System.out.println("Now I know " + newEdgeName);
    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (IOException e) {
      e.printStackTrace();
      System.out.println("Reply to hello_message has failed its transmission to " + newEdgeName);
    }
  }
}
