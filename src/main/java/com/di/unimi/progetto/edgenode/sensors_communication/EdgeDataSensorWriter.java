package com.di.unimi.progetto.edgenode.sensors_communication;

import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.net.SocketException;

/* Questa classe realizza un thread per l'ascolto di uno stream continuo di dati di uno specifico sensore.
 *  Ho tanti EdgeDataSensorWriter quanti sono i sensori connessi con il nodo edge a cui faccio riferimento.
 *  Appena ricevo una misurazione la metto nel sensorsDataBuffer condiviso
 */
public class EdgeDataSensorWriter extends Thread {

  private EdgeSensorsDataBuffer sensorsDataBuffer;
  private Socket connectedSensorSocket;
  private BufferedReader inFromSensor;
  private Gson gson;

  public EdgeDataSensorWriter(Socket workingSensorSocket) {
    sensorsDataBuffer = EdgeSensorsDataBuffer.getInstance();
    try {
      connectedSensorSocket = workingSensorSocket;
      inFromSensor =
          new BufferedReader(new InputStreamReader(connectedSensorSocket.getInputStream()));
    } catch (IOException e) {
      e.printStackTrace();
    }

    gson = new GsonBuilder().create();
  }

  public void run() {
    /* Mi metto in attesa di dati di un sensore, ogni dato recepito lo metto nel sensorsDataBuffer condiviso
     * Continuo a ciclare finche' il nodo edge e' attivo nella rete */
    while (EdgePanel.edgeAlive) {
      try {
        String jsonMeasurement = inFromSensor.readLine();

        /* In questo caso esco dal ciclo */
        if (jsonMeasurement == null) throw new SocketException();

        Measurement measurement = gson.fromJson(jsonMeasurement, Measurement.class);
        sensorsDataBuffer.put(measurement);
      } catch (SocketException e) {
        //      System.out.println("The sensor has changed edge node, goodbye!");
        break;
      } catch (IOException e) {
        e.printStackTrace();
        break;
      }
    }
    /* Uscendo dal ciclo, che sia per errori o inattività del nodo chiudo la socket col sensore */
    try {
      connectedSensorSocket.close();
      //   System.out.println("EdgeDataSensorWriter stopped");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
