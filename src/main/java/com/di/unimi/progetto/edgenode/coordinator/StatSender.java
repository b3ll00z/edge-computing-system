package com.di.unimi.progetto.edgenode.coordinator;

import com.di.unimi.progetto.beans.CityStat;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public abstract class StatSender extends Thread {

  protected String name;
  protected String jsonCityStat;
  protected Gson gson;

  public StatSender(String nodeId, Measurement measurement) {
    name = nodeId;
    gson = new GsonBuilder().create();
    jsonCityStat = gson.toJson(new CityStat(measurement.getValue(), measurement.getTimestamp()));
  }

  public abstract void run();
}
