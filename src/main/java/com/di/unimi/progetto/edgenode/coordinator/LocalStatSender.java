package com.di.unimi.progetto.edgenode.coordinator;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.simulator.Measurement;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import javax.ws.rs.core.MediaType;

public class LocalStatSender extends StatSender {

  public LocalStatSender(String nodeId, Measurement measurement) {
    super(nodeId, measurement);
  }

  @Override
  public void run() {
    /* Imposto chiamata rest al server cloud per la trasmissione della statistica locale */
    Client client = Client.create();

    WebResource cloudServerResource = client.resource("http://localhost:8888/cloud/");
    ClientResponse response =
        cloudServerResource
            .path("edge/local/" + name)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, jsonCityStat);

    // System.out.println(response);
    /* Not found, tolgo il nodo */
    if (response.getStatus() == 11) EdgeNode.getInstance().removeEdge(name);
  }
}
