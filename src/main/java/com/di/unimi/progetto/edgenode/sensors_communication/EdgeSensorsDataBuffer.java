package com.di.unimi.progetto.edgenode.sensors_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.simulator.Measurement;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class EdgeSensorsDataBuffer {

  private static EdgeSensorsDataBuffer instance = null;
  private EdgeNode node;
  private List<Measurement> buffer;

  private EdgeSensorsDataBuffer() {
    node = EdgeNode.getInstance();
    buffer = new ArrayList<Measurement>(40);
  }

  public static synchronized EdgeSensorsDataBuffer getInstance() {
    if (instance == null) instance = new EdgeSensorsDataBuffer();

    return instance;
  }

  /* Metodo usato da ogni thread che rileva misure da uno specifico sensore e lo mette nel buffer */
  public synchronized void put(Measurement message) {
    if (buffer.size() < 40) buffer.add(message);

    notifyAll();
  }

  public synchronized Measurement makeLocalStat() {

    Measurement localStat = null;

    while (buffer.size() < 40) {
      try {
        wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

    if (buffer.size() == 40) {
      /* Ai fini della tecnica di gestione dati Sliding Windows con overlap del 50%
       * prendo le due metà del buffer e uso la seconda per rimetterla nel buffer, previo svuotamento */
      List<Measurement> firstHalf = new ArrayList<>(buffer.subList(0, 20));
      List<Measurement> secondHalf = new ArrayList<>(buffer.subList(20, buffer.size()));
      buffer.clear();
      buffer.addAll(secondHalf);

      localStat = computeStat(firstHalf, secondHalf);
    }

    return localStat;
  }

  private Measurement computeStat(List<Measurement> firstHalf, List<Measurement> secondHalf) {

    double sum = 0;

    for (Measurement m : firstHalf) sum += m.getValue();

    for (Measurement m : secondHalf) sum += m.getValue();

    double avg = sum / 40;

    /* Calcolo del timestamp nel momento in cui ho calcolato la media */
    Calendar c = Calendar.getInstance();
    c.set(Calendar.HOUR_OF_DAY, 0);
    c.set(Calendar.MINUTE, 0);
    c.set(Calendar.SECOND, 0);
    c.set(Calendar.MILLISECOND, 0);

    long timestamp = System.currentTimeMillis() - c.getTimeInMillis();

    return new Measurement(node.getId(), "LocalStat-PM10", avg, timestamp);
  }
}
