package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.edgenode.coordinator.CoordinatorStatsHandler;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;
import java.util.ArrayList;
import java.util.List;

public class EdgeBecomeCoordinatorHandler extends Thread {

  private EdgeNode node;
  private EdgeDataSensorsReader dataSensorsReader;
  private List<EdgeInfo> allEdges;

  public EdgeBecomeCoordinatorHandler() {
    node = EdgeNode.getInstance();
    dataSensorsReader = EdgeDataSensorsReader.getInstance();
    allEdges = node.getAllEdges();
  }

  public void run() {

    /* Avvio thread per calcolo media globale */
    EdgeMessageReader.coordinatorStatsHandler = new CoordinatorStatsHandler();
    EdgeMessageReader.coordinatorStatsHandler.start();
    EdgePanel.coordinator = true;

    /* Refresh del nuovo coordinatore */
    node.setCoordinatorName(node.getId());

    /* Imposto il consumatore di dati di sensore per produrre statistiche locali con le mie coordinate */
    dataSensorsReader.refreshCoordinatorInfo(node.getIp(), node.getPortForEdges());

    /* Termino elezione */
    dataSensorsReader.stopElection();

    /* Costruisco lista di sender */
    List<EdgeCoordinatorSender> coordinatorSenders = new ArrayList<>();

    for (EdgeInfo edgeToNotify : allEdges)
      coordinatorSenders.add(new EdgeCoordinatorSender(edgeToNotify));

    /* Lancio i thread e attendo la terminazione di ognuno di loro prima di proseguire */
    try {
      for (EdgeCoordinatorSender sender : coordinatorSenders) sender.start();

      for (EdgeCoordinatorSender sender : coordinatorSenders) sender.join();

    } catch (InterruptedException e) {
      e.printStackTrace();
    }
    System.out.println("I'm the new coordinator");
    // System.out.println("ALL CORDINATOR MESSAGES SENT");
  }
}
