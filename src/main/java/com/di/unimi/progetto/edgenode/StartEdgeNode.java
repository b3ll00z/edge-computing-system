package com.di.unimi.progetto.edgenode;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.edges_communication.EdgeWelcomeHandler;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.util.List;

public class StartEdgeNode {

  public static void main(String[] args) throws IOException {

    // INIZIALIZZAZIONE
    BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
    ServerSocket socketForEdges = null;
    ServerSocket socketForSensors = null;

    System.out.print("Name: ");
    String name = in.readLine();

    int edgePort;

    // Controllo disponibilità porte
    do {
      try {
        System.out.print("Port for edge nodes: ");
        edgePort = Integer.parseInt(in.readLine());
        socketForEdges = new ServerSocket(edgePort);
      } catch (IllegalArgumentException | IOException e) {
        edgePort = 0;
      }
    } while (edgePort == 0);

    int sensorPort;

    do {
      try {
        System.out.print("Port for sensors: ");
        sensorPort = Integer.parseInt(in.readLine());
        socketForSensors = new ServerSocket(sensorPort);
      } catch (IllegalArgumentException | IOException e) {
        sensorPort = 0;
      }
    } while (sensorPort == 0);

    /* Creazione nuovo edge, va direttamente ad instaziare il singleton, che sarà reperito da altri thread */
    EdgeNode.createEdgeNode(
        name, InetAddress.getLocalHost().getHostAddress(), edgePort, sensorPort);
    EdgeNode createdNode = EdgeNode.getInstance();
    createdNode.setSocketForEdges(socketForEdges);
    createdNode.setSocketForSensors(socketForSensors);

    /* Registrazione effettiva al server cloud tramite un assistant */
    List<EdgeInfo> city = EdgeAssistant.addEdgeNodeInCity();

    /* Rilevazione di errori nella chiamata precedente al server cloud */
    if (city == null) {
      System.out.println("No way, please retry in another moment");
      System.exit(1);
    }

    /* Avvio thread per gestire attività del nodo, più precisamente si tratta di un controller per farlo stoppare */
    new EdgePanel().start();

    /* Avvio thread per gestire la risposta del server */
    new EdgeWelcomeHandler(city).start();
  }
}
