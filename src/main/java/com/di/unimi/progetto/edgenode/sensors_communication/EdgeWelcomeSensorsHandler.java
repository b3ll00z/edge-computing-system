package com.di.unimi.progetto.edgenode.sensors_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/* Con questa classe avvio un thread dispatcher che si mette in ascolto di connessioni di nuovi sensori */
public class EdgeWelcomeSensorsHandler extends Thread {

  public static EdgeWelcomeSensorsHandler instance = null;

  private EdgeNode node;
  private ServerSocket socketForSensors;

  private EdgeWelcomeSensorsHandler() {
    node = EdgeNode.getInstance();
    socketForSensors = node.getSocketForSensors();
  }

  public static synchronized EdgeWelcomeSensorsHandler getInstance() {
    if (instance == null) instance = new EdgeWelcomeSensorsHandler();

    return instance;
  }

  public void run() {
    /* Avvio il thread che si occupa di leggere dal buffer i dati di sensori della mia zona */
    EdgeDataSensorsReader.getInstance().start();

    /* Avvio dispatcher per ricevere dati di sensori finche' il nodo e' in attività */
    while (EdgePanel.edgeAlive) {
      try {
        Socket workingSensorSocket = socketForSensors.accept();
        new EdgeDataSensorWriter(workingSensorSocket).start();
      } catch (IOException e) {
        e.printStackTrace();
      }
    }

    /* Chiudo Server Socket del nodo */
    try {
      socketForSensors.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
