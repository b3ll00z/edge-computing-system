package com.di.unimi.progetto.edgenode;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.io.IOException;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;

/*
   L'intento di un EdgeAssistant e' quello di fare da delegato per il nodo edge a cui e' vincolato.
   Provvede alle chiamate REST al ServerCloud
   Implementa il pattern Singleton visto che serve una e una sola istanza di questa classe
*/
public class EdgeAssistant {

  public static List<EdgeInfo> addEdgeNodeInCity() throws IOException {

    /* Predispongo via java una chiamata REST al server per registrare il nodo */
    Client client = Client.create();
    WebResource cloudServerResource = client.resource("http://localhost:8888/cloud/edge");

    Gson gson = new GsonBuilder().create();
    Type edgesListType = new TypeToken<ArrayList<EdgeInfo>>() {}.getType();

    /* Rilevo il singleton del mio edge node */
    EdgeNode node = EdgeNode.getInstance();

    int attempts = 10;

    while (attempts > 0) {
      /* il nodo edge genera una posizione random e tenta di inserirsi nella griglia comunicando col server cloud */
      node.generatePositionInCity();

      String jsonEdgeNode =
          gson.toJson(
              new EdgeInfo(
                  node.getId(),
                  node.getIp(),
                  node.getPortForEdges(),
                  node.getPortForSensors(),
                  node.getPositionInCity()));

      ClientResponse response =
          cloudServerResource
              .type(MediaType.APPLICATION_JSON)
              .accept(MediaType.APPLICATION_JSON)
              .post(ClientResponse.class, jsonEdgeNode);

      System.out.println(response);

      /* nodo accettato, restituisco la lista dei nodi nella città */
      if (response.getStatus() == 200) {
        String jsonResponseEntity = response.getEntity(String.class);
        return gson.fromJson(jsonResponseEntity, edgesListType);
      }
      /* posizione non accettata pertanto decremento i tentativi */
      else if (response.getStatus() == 12) attempts--;
      /* nodo già presente nella rete, lo segnalo via stdout ed esco */
      else if (response.getStatus() == 13) {
        System.out.println(response.getEntity(String.class));
        return null;
      }
      /* ogni altro errore interno non viene accettato */
      else return null;
    }

    /* se arrivo qui significa che ho esaurito tutti i tentativi */
    return null;
  }

  public static void stopActivity() {

    /* Predispongo via java una chiamata REST al server per togliere il nodo dalla rete */
    EdgeNode node = EdgeNode.getInstance();
    Client client = Client.create();

    WebResource cloudServerResource = client.resource("http://localhost:8888/cloud/");
    ClientResponse response =
        cloudServerResource.path("edge/" + node.getId()).delete(ClientResponse.class);

    System.out.println(response.toString());
  }
}
