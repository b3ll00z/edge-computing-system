package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class EdgeMeasurementProducer extends Thread {

  private EdgeNode node;
  private String senderName;
  private String senderIP;
  private int senderPort;
  private Measurement measurementToCollect;

  public EdgeMeasurementProducer(
      String name, String ip, int portForEdges, Measurement measurement) {
    node = EdgeNode.getInstance();
    senderName = name;
    senderIP = ip;
    senderPort = portForEdges;
    measurementToCollect = measurement;
  }

  public void run() {

    /* Aggiungo statistica locale nel buffer della componente che calcola stastica globale ed invia tutto al server */
    EdgeMessageReader.coordinatorStatsHandler.addNewLocalStat(senderName, measurementToCollect);

    Gson gson = new GsonBuilder().create();

    /* Recupero l'ultima coordinata globale e la impacchetto in un Message di risposta */
    Measurement lastGlobalStat = EdgeMessageReader.coordinatorStatsHandler.getLastGlobalStat();

    EdgeMessage lastGlobalStatMessage =
        new EdgeMessage(node.getId(), EdgeMessage.Content.MEASUREMENT);
    lastGlobalStatMessage.from(node.getIp());
    lastGlobalStatMessage.setMeasurement(lastGlobalStat);

    String jsonLastGlobalStatMessage = gson.toJson(lastGlobalStatMessage);

    /* Invio messaggio di statistica globale, se si verificano errori di connessione rimuovo il destinatario
     * dalla mia conoscenza della rete P2P */
    try {
      Socket socket = new Socket(senderIP, senderPort);
      DataOutputStream outToEdge = new DataOutputStream(socket.getOutputStream());
      outToEdge.writeBytes(jsonLastGlobalStatMessage + "\n");
      outToEdge.flush();
    } catch (IOException e) {
      System.out.println(senderName + " has left the city!");

      if (!senderName.equals(node.getId())) EdgeNode.getInstance().removeEdge(senderName);
    }
  }
}
