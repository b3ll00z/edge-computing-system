package com.di.unimi.progetto.edgenode.coordinator;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.simulator.Measurement;
import java.util.*;

/* Questa classe serve a un nodo edge coordinatore per ricevere statistiche locali dai nodi edge
 * Viene utilizzata anche per rispondere col l'ultima statistica globale a seguito di una statistica locale ricevuta */
public class CoordinatorStatsHandler extends Thread {

  private EdgeNode node;
  private Map<String, Queue<Measurement>> localStatReceived;
  private Map<String, Measurement> localStatToTransmit;
  private Measurement lastGlobalStat;
  private Calendar midnight;

  public CoordinatorStatsHandler() {
    node = EdgeNode.getInstance();
    localStatReceived = new HashMap<>();
    lastGlobalStat = new Measurement(node.getId(), "GlobalStat-PM10", 0, 0);
    midnight = Calendar.getInstance();
    midnight.set(Calendar.HOUR_OF_DAY, 0);
    midnight.set(Calendar.MINUTE, 0);
    midnight.set(Calendar.SECOND, 0);
    midnight.set(Calendar.MILLISECOND, 0);
  }

  /* Metodo per raccogliere le statistiche locali, accedendo alla struttura dati contenitore in modo sincronizzato */
  public void addNewLocalStat(String senderEdge, Measurement senderEdgeLocalStat) {
    synchronized (localStatReceived) {
      if (!localStatReceived.containsKey(senderEdge))
        localStatReceived.put(senderEdge, new PriorityQueue<>());

      Queue<Measurement> edgeStats = localStatReceived.get(senderEdge);
      edgeStats.add(senderEdgeLocalStat);
      localStatReceived.put(senderEdge, edgeStats);
    }
  }

  /* Copia di backup della mappa */
  private Map<String, Queue<Measurement>> getCopyOfLocalStats() {

    synchronized (localStatReceived) {
      return new HashMap<String, Queue<Measurement>>(localStatReceived);
    }
  }

  public void clearStats() {
    synchronized (localStatReceived) {
      localStatReceived.clear();
    }
  }

  private void makeGlobalStat() {

    /* Acquisizione statistiche locali fino a questo istante pervenute */
    Map<String, Queue<Measurement>> copy = getCopyOfLocalStats();

    /* Finche' non ho statistiche mando a vuoto l'operazione */
    if (copy.isEmpty()) return;

    /* Svuoto cache delle statistiche locali */
    clearStats();

    Map<String, Measurement> localToTransmit = new HashMap<>();

    /* Ad ogni nodo edge sono collegate varie statistiche locali
     * Per ogni lista calcolo la media e la metto in una seconda lista, che verrà usata per le globali */
    for (String edge : copy.keySet()) {

      double sum = 0;
      double avg = 0;

      Queue<Measurement> localMeasurements = copy.get(edge);

      for (Measurement localStat : localMeasurements) sum += localStat.getValue();

      avg = sum / localMeasurements.size();
      localToTransmit.put(
          edge,
          new Measurement(
              edge,
              "LocalStat-PM10",
              avg,
              System.currentTimeMillis() - midnight.getTimeInMillis()));
    }

    localStatToTransmit = localToTransmit;

    double avg = 0;
    double sum = 0;

    for (Measurement avgMeasurement : localToTransmit.values()) sum += avgMeasurement.getValue();

    avg = sum / localToTransmit.size();
    /* Acquisizione timestamp e preparazione statistica globale */
    long coordinatorTimestamp = System.currentTimeMillis() - midnight.getTimeInMillis();
    Measurement globalStat =
        new Measurement(node.getId(), "GlobalStat-PM10", avg, coordinatorTimestamp);

    synchronized (lastGlobalStat) {
      lastGlobalStat = globalStat;
    }
  }

  public Measurement getLastGlobalStat() {
    synchronized (lastGlobalStat) {
      return new Measurement(
          lastGlobalStat.getId(),
          lastGlobalStat.getType(),
          lastGlobalStat.getValue(),
          lastGlobalStat.getTimestamp());
    }
  }

  private void sendResultsToServer() {
    Measurement copyOfLastGlobalStat = getLastGlobalStat();

    if (localStatToTransmit.isEmpty()) {
      // System.out.println("Still nothing to transmit");
      return;
    }

    List<StatSender> statSenders = new ArrayList<>();

    /* Può accadere da un momento all'altro che il nodo esca intenzionalmente */

    for (String nodeId : localStatToTransmit.keySet()) {
      if (!nodeId.equals(node.getId()))
        statSenders.add(new LocalStatSender(nodeId, localStatToTransmit.get(nodeId)));
    }
    /* Invio statistica globale al server */
    statSenders.add(new GlobalStatSender(node.getId(), copyOfLastGlobalStat));

    try {
      /* Apro pool di thread per trasmettere statistiche locali, uno per nodo cui ho ricevuto statistica */
      for (StatSender statSender : statSenders) statSender.start();

      /* Apro pool di thread per trasmettere statistiche locali, uno per nodo cui ho ricevuto statistica */
      for (StatSender statSender : statSenders) statSender.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void run() {
    /* Ogni 5 secondi calcolo le statistiche locali ricevute e le trasmetto al server */
    while (EdgePanel.edgeAlive && EdgePanel.coordinator) {
      try {
        Thread.sleep(5000);
        makeGlobalStat();
        sendResultsToServer();
      } catch (NullPointerException e) {
        System.out.println("Still nothing to send");
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }
}
