package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class EdgeElectionSender extends Thread {

  private EdgeNode node;
  private String edgeToNotify;
  private String edgeHostToNotify;
  private int edgePortToNotify;
  private EdgeCounter counter;

  public EdgeElectionSender(String edgeName, String edgeIp, int edgePort, EdgeCounter c) {
    node = EdgeNode.getInstance();
    edgeToNotify = edgeName;
    edgeHostToNotify = edgeIp;
    edgePortToNotify = edgePort;
    counter = c;
  }

  public void run() {
    try {
      /* Preparazione messaggio di elezione */
      EdgeMessage electionMessage = new EdgeMessage(node.getId(), EdgeMessage.Content.ELECTION);
      electionMessage.from(node.getIp());
      electionMessage.setPortForEdges(node.getPortForEdges());

      Gson gson = new GsonBuilder().create();
      String jsonElectionMessage = gson.toJson(electionMessage);

      /* Apertura socket verso il nodo cui devo comunicargli elezione ed invio messaggio */
      Socket socket = new Socket(edgeHostToNotify, edgePortToNotify);
      DataOutputStream outToEdge = new DataOutputStream(socket.getOutputStream());
      outToEdge.writeBytes(jsonElectionMessage + "\n");
      outToEdge.flush();

      System.out.println("Sent election_message to " + edgeToNotify);

    } catch (IOException e) {
      System.out.println("Unable to send election_message to " + edgeToNotify);

      /* Provvedo a rimuovere il nodo */
      node.removeEdge(edgeToNotify);
      System.out.println(edgeToNotify + " isn't in the city anymore");
      counter.decrement();
    }
  }
}
