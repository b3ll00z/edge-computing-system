package com.di.unimi.progetto.edgenode;

import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import java.util.PriorityQueue;
import java.util.Queue;

/* Essenzialmente l'uso di questa classe serve per la funzionalità di Pannelo, dove tramite menu interattivo
 * si possono consultare statistiche sia locali del nodo edge, sia globali direttamente dal coordinatore.
 * Inoltre si ha la possibilitò di far uscire il nodo dalla rete */
public class EdgePanel extends Thread {

  public static volatile boolean edgeAlive = true;
  public static volatile boolean coordinator = false;
  private static Queue<Measurement> lastLocalStat = new PriorityQueue<>(40);
  private static Queue<Measurement> lastGlobalStat = new PriorityQueue<>(40);

  private Gson gson = new GsonBuilder().setPrettyPrinting().create();
  private Type token = new TypeToken<ArrayList<Measurement>>() {}.getType();

  public void run() {
    try {
      BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
      System.out.println("***** EDGE ACTIVATED, PLEASE WRITE 'quit' TO STOP IT *****");
      System.out.println();
      while (true) {
        System.out.println("1. Show last local stat");
        System.out.println("2. Show last global stat");
        System.out.println("3. Stop edge");

        String exit = in.readLine();

        if (exit.equals("1")) {
          System.out.print("How many local stats? ");
          int n = setAmountOfStats(in);

          Queue<Measurement> localStats = getLastLocalStat();

          if (!localStats.isEmpty()) {
            System.out.println("Here's last local stats");
            printStats(localStats, n);
          }
        }
        if (exit.equals("2")) {
          System.out.print("How many global stats? ");
          int n = setAmountOfStats(in);

          Queue<Measurement> globalStats = getLastGlobalStat();

          if (!globalStats.isEmpty()) {
            System.out.println("Here's last global stats");
            printStats(globalStats, n);
          }
        }
        if (exit.equals("3")) {
          logout();
          break;
        }
      }
    } catch (Exception e) {
      logout();
    }
  }

  private Queue<Measurement> getLastLocalStat() {
    synchronized (lastLocalStat) {
      return new PriorityQueue<>(lastLocalStat);
    }
  }

  private Queue<Measurement> getLastGlobalStat() {
    synchronized (lastGlobalStat) {
      return new PriorityQueue<>(lastGlobalStat);
    }
  }

  public static void addLocalStat(Measurement localStat) {
    synchronized (lastLocalStat) {
      if (lastLocalStat.size() == 40) lastLocalStat.poll();

      lastLocalStat.add(localStat);
    }
  }

  public static void addGlobalStat(Measurement globalStat) {
    synchronized (lastGlobalStat) {
      if (lastGlobalStat.size() == 40) lastGlobalStat.poll();

      lastGlobalStat.add(globalStat);
    }
  }

  private static int setAmountOfStats(BufferedReader br) throws IOException {
    int n;
    do {
      try {
        n = Integer.parseInt(br.readLine());
      } catch (NumberFormatException e) {
        n = 0;
        System.out.print("How many statistics? ");
      }
    } while (n == 0);
    return n;
  }

  private void printStats(Queue<Measurement> queue, int sizeToPrint) {

    int limit = (queue.size() < sizeToPrint) ? queue.size() : sizeToPrint;

    List<Measurement> queueListed = new ArrayList<>(queue);
    System.out.println(gson.toJson(queueListed.subList(0, limit), token));
  }

  private void logout() {
    /* Wrapper di chiamata al server per essere rimosso dalla griglia della città */
    EdgeAssistant.stopActivity();
    edgeAlive = false;
    coordinator = false;
    System.out.println("***** EDGE STOPPED *****");
    System.exit(0);
  }
}
