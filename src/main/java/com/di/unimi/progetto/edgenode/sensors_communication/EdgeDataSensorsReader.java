package com.di.unimi.progetto.edgenode.sensors_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.edgenode.edges_communication.EdgeBecomeCoordinatorHandler;
import com.di.unimi.progetto.edgenode.edges_communication.EdgeElectionStarter;
import com.di.unimi.progetto.edgenode.edges_communication.EdgeMessage;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

/* Nella mia scelta implementativa ho deciso che per la gestione dei dati di sensore regolo il tutto con
 *  tanti produttori quanti sono i sensori a me collegati e un solo consumatore. Questa classe pertanto si avvale
 *  del pattern Singleton, in quanto e' richiesta la creazione di un solo consumatore ed e' necessario applicare
 *  in caso di elezione di un nuovo coordinatore, dei cambiamenti che devono essere immediatamente visibili:
 *  vale a dire le coordinate del nuovo coordinatore, a cui verranno inviate le statistice che ho prodotto localmente.
 */
public class EdgeDataSensorsReader extends Thread {

  private static EdgeDataSensorsReader instance = null;

  private EdgeNode node;
  private EdgeSensorsDataBuffer sensorsDataBuffer;
  private Gson gson;
  private volatile String coordinatorHost = null;
  private volatile int coordinatorPort = 0;
  private volatile boolean electionOnAir = false;

  private Object electionMonitor = new Object();

  private EdgeDataSensorsReader() {
    node = EdgeNode.getInstance();
    sensorsDataBuffer = EdgeSensorsDataBuffer.getInstance();
    gson = new GsonBuilder().create();
  }

  public static synchronized EdgeDataSensorsReader getInstance() {
    if (instance == null) instance = new EdgeDataSensorsReader();

    return instance;
  }

  public void refreshCoordinatorInfo(String newCoordinatorHost, int newCoordinatorPort) {
    coordinatorHost = newCoordinatorHost;
    coordinatorPort = newCoordinatorPort;
  }

  public void clearCoordinator() {
    coordinatorHost = null;
    coordinatorPort = 0;
  }

  public void startElection() {
    synchronized (electionMonitor) {
      electionOnAir = true;
    }
  }

  public void stopElection() {
    synchronized (electionMonitor) {
      electionOnAir = false;
      electionMonitor.notifyAll();
    }
  }

  public boolean isElectionInCourse() {
    return electionOnAir;
  }

  public void waitElectionTermination() {
    synchronized (electionMonitor) {
      try {
        while (electionOnAir) electionMonitor.wait();
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }
  }

  public void run() {

    while (EdgePanel.edgeAlive) {
      /* Acquisico statistica locale */
      Measurement localStatToSend = sensorsDataBuffer.makeLocalStat();
      sendToCoordinator(localStatToSend);
    }
  }

  private int timesStatPutOnBuffer = 0;

  private void sendToCoordinator(Measurement localStat) {

    /* Converto statistica in formato json e la trasmetto al coordinatore */
    String jsonLocalStat = gson.toJson(localStat);
    //   System.out.println("Local stat produced: " + jsonLocalStat);
    EdgePanel.addLocalStat(localStat);

    /* Mi connetto al coordinatore, qui rilevo la sua assenza e indico un elezione
     * Se e' in corso un'elezione, rimetto le statistiche locali nel sensorsDataBuffer. Assumo dopo 50 volte
     * che rimetto nel buffer di essere rimasto l'unico e mi autoproclamo coordinatore.
     * Altrimenti le trasmetto al coordinatore */
    if (isElectionInCourse()) {
      // System.out.println("Local stat put into buffer");
      sensorsDataBuffer.put(localStat);
      timesStatPutOnBuffer += 1;

      if (timesStatPutOnBuffer == 50) {
        timesStatPutOnBuffer = 0;
        try {
          EdgeBecomeCoordinatorHandler handler = new EdgeBecomeCoordinatorHandler();
          handler.setPriority(MAX_PRIORITY);
          handler.start();
          handler.join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    } else {
      try {
        /* Preparazione messaggio contenente statistica locale */
        EdgeMessage localStatMessage =
            new EdgeMessage(node.getId(), EdgeMessage.Content.MEASUREMENT);
        localStatMessage.from(node.getIp());
        localStatMessage.setPortForEdges(node.getPortForEdges());
        localStatMessage.setMeasurement(localStat);

        String jsonLocalStatMessage = gson.toJson(localStatMessage);

        /* Invio statistica locale al coordinatore */
        Socket socketToCoordinator = new Socket(coordinatorHost, coordinatorPort);
        DataOutputStream outToCoordinator =
            new DataOutputStream(socketToCoordinator.getOutputStream());
        outToCoordinator.writeBytes(jsonLocalStatMessage + "\n");
        outToCoordinator.flush();

        timesStatPutOnBuffer = 0;
      } catch (UnknownHostException e) {
        e.printStackTrace();
      } catch (IOException e) {
        /* Rimuovo il coordinatore dalla mia conoscenza della rete P2P */
        node.removeEdge(node.getCoordinatorName());
        electionLauncher();
      }
    }
  }

  public void electionLauncher() {
    try {
      clearCoordinator();
      startElection();
      System.out.println("Coordinator not found, election started!");
      EdgeElectionStarter starter = new EdgeElectionStarter();
      starter.setPriority(MAX_PRIORITY);
      starter.start();
      starter.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }
}
