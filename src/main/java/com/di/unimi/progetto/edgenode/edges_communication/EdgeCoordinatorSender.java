package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class EdgeCoordinatorSender extends Thread {

  private EdgeNode node;
  private EdgeInfo edgeToNotify;

  public EdgeCoordinatorSender(EdgeInfo edge) {
    node = EdgeNode.getInstance();
    edgeToNotify = edge;
  }

  public void run() {
    try {
      /* Preparazione messaggio di elezione */
      EdgeMessage newCoordinatorMessage =
          new EdgeMessage(node.getId(), EdgeMessage.Content.COORDINATOR);
      newCoordinatorMessage.from(node.getIp());
      newCoordinatorMessage.setPortForEdges(node.getPortForEdges());

      Gson gson = new GsonBuilder().create();
      String jsonNewCoordinatorMessage = gson.toJson(newCoordinatorMessage);

      /* Apertura socket verso il nodo cui devo comunicargli elezione ed invio messaggio */
      Socket socket = new Socket(edgeToNotify.getIP(), edgeToNotify.getPortForEdgeNodes());
      DataOutputStream outToEdge = new DataOutputStream(socket.getOutputStream());
      outToEdge.writeBytes(jsonNewCoordinatorMessage + "\n");
      outToEdge.flush();

      System.out.println("Sent coordinator_message to " + edgeToNotify.getName());
    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (IOException e) {
      System.out.println("Unable to send coordinator_message to " + edgeToNotify.getName());
      /* Provvedo a rimuovere il nodo */
      node.removeEdge(edgeToNotify.getName());
    }
  }
}
