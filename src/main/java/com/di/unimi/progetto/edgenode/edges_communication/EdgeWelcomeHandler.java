package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.edgenode.coordinator.CoordinatorStatsHandler;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

/*
   EdgeWelcomeHandler rappresenta per uno specifico nodo edge un in attesa di connessioni in arrivo per messaggi da nodi edge.

   Al momento della creazione viene costruita la topologia logica per il nodo, sulla base dello stato attuale
   della città che mi è stato fornito dal server cloud in fase di registrazione.

   Se rilevo la mia sola presenza in città mi autoproclamo coordinatore e passo all'ascolto di connessioni entranti.

   Altrimenti per ogni nodo edge che rilevo dallo stato della città mi connetto via socket, invio un opportuno
   messaggio di ingresso nella rete e aspetto un ack di ricezione, il tutto gestito da opportuni handler; dopodichè mi
   metto in attesa di connessioni entranti.
*/
public class EdgeWelcomeHandler extends Thread {

  private EdgeNode node;
  private ServerSocket socketForEdges;
  private ServerSocket socketForSensors;

  public EdgeWelcomeHandler(List<EdgeInfo> city) {
    node = EdgeNode.getInstance();
    socketForEdges = node.getSocketForEdges();
    socketForSensors = node.getSocketForSensors();

    createEdgesKnowledge(city);
  }

  private void createEdgesKnowledge(List<EdgeInfo> city) {

    /* Se la risposta contiene un unico elemento, vuol dire che sono il primo nodo edge ergo divento coordinatore
     * Inoltre avvio il thread che il coordinatore sfrutta per inviare statistiche globali e locali al cloud */
    if (city.size() == 1) {
      node.setCoordinatorName(node.getId());

      /* Imposto il consumatore di dati di sensore per produrre statistiche locali con le mie coordinate */
      EdgeDataSensorsReader.getInstance()
          .refreshCoordinatorInfo(node.getIp(), node.getPortForEdges());

      /* Avvio thread per calcolo media globale */
      EdgeMessageReader.coordinatorStatsHandler = new CoordinatorStatsHandler();
      EdgeMessageReader.coordinatorStatsHandler.start();
      EdgePanel.coordinator = true;

      System.out.println("The city is empty, I'm the new coordinator");
      System.out.println("Stats handler started!");
      return;
    }

    /* Quindi scorro la lista e mi collego coi nodi edge in ascolto e apro un pool di thread che in parallelo
     * interrogano i nodi edge della rete. Se sto presentandomi proprio quando si sta eleggendo un coordinatore
     * attendo la fine dell'elezione */
    List<EdgeHelloSender> helloSenders = new ArrayList<>();

    try {
      for (EdgeInfo info : city)
        if (!node.getId().equals(info.getName())) helloSenders.add(new EdgeHelloSender(info));

      for (EdgeHelloSender helloSender : helloSenders) helloSender.start();

      for (EdgeHelloSender helloSender : helloSenders) helloSender.join();
    } catch (InterruptedException e) {
      e.printStackTrace();
    }
  }

  public void run() {
    /* Da questo momento in avanti avvio il dispatcher sulla porta per colloquiare con nodi edge
     * Da qui arrivano messaggi di ingresso di un nuovo nodo nella rete, di elezione, di ack e di nuovo coordinatore
     * Inoltre qualora il nodo edge diventasse coordinatore, anche i messaggi contenenti statistiche locali di altri
     * nodi edge passano da qui */
    new EdgeMessageReader().start();

    while (EdgePanel.edgeAlive) {
      try {
        Socket workingSocket = socketForEdges.accept();
        new EdgeMessageWriter(workingSocket).start();
      } catch (IOException e) {
        break;
      }
    }
    /* Chiudo Server Socket del nodo */
    try {
      socketForEdges.close();
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
}
