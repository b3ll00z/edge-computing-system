package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;

public class EdgeHelloWaitingRoom extends Thread {

  private String edgeToInsert;
  private String edgeToInsertIp;
  private int edgeToInsertPort;
  private EdgeDataSensorsReader dataSensorsReader;

  public EdgeHelloWaitingRoom(
      String name, String ip, int port, EdgeDataSensorsReader sensorsReader) {
    edgeToInsert = name;
    edgeToInsertIp = ip;
    edgeToInsertPort = port;
    dataSensorsReader = sensorsReader;
  }

  public void run() {
    /* Entrato in questo thread so che e' in corso un'elezione quindi attendo che finisca
     * prima di rispondere a un nuovo nodo entrante */
    dataSensorsReader.waitElectionTermination();
    new EdgeHelloRespondent(edgeToInsert, edgeToInsertIp, edgeToInsertPort, dataSensorsReader)
        .start();
  }
}
