package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.edgenode.EdgeNode;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.UnknownHostException;

public class EdgeOkSender extends Thread {

  private EdgeNode node;
  private String edgeToNotify;
  private String edgeHostToNotify;
  private int edgePortToNotify;

  public EdgeOkSender(String edgeName, String edgeIp, int edgePort) {
    node = EdgeNode.getInstance();
    edgeToNotify = edgeName;
    edgeHostToNotify = edgeIp;
    edgePortToNotify = edgePort;
  }

  public void run() {
    try {
      /* Preparazione messaggio di OK a seguito di un elezione */
      EdgeMessage electionMessage = new EdgeMessage(node.getId(), EdgeMessage.Content.OK);
      electionMessage.from(node.getIp());
      electionMessage.setPortForEdges(node.getPortForEdges());

      Gson gson = new GsonBuilder().create();
      String jsonElectionMessage = gson.toJson(electionMessage);

      /* Apertura socket verso il nodo cui devo comunicargli elezione ed invio messaggio */
      Socket socket = new Socket(edgeHostToNotify, edgePortToNotify);
      DataOutputStream outToEdge = new DataOutputStream(socket.getOutputStream());
      outToEdge.writeBytes(jsonElectionMessage + "\n");
      outToEdge.flush();

      System.out.println("Sent ok_message to " + edgeToNotify);
    } catch (UnknownHostException e) {
      e.printStackTrace();
    } catch (IOException e) {
      System.out.println("Unable to send OK to " + edgeHostToNotify);
      /* Provvedo a rimuovere il nodo */
      node.removeEdge(edgeToNotify);
    }
  }
}
