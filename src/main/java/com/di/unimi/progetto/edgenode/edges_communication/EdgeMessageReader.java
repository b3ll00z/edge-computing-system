package com.di.unimi.progetto.edgenode.edges_communication;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgeNode;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.edgenode.coordinator.CoordinatorStatsHandler;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeDataSensorsReader;
import com.di.unimi.progetto.edgenode.sensors_communication.EdgeWelcomeSensorsHandler;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

public class EdgeMessageReader extends Thread {

  protected static volatile CoordinatorStatsHandler coordinatorStatsHandler;

  private EdgeNode node;
  private EdgeDataSensorsReader dataSensorsReader;
  private EdgeMessageBuffer bufferOfMessages;
  private Gson gson;

  public EdgeMessageReader() {
    node = EdgeNode.getInstance();
    dataSensorsReader = EdgeDataSensorsReader.getInstance();
    bufferOfMessages = EdgeMessageBuffer.getInstance();
    gson = new GsonBuilder().create();
  }

  public void run() {
    /* Finche' il nodo e' in attività prendo dal buffer un messaggio alla volta e ne analizzo il contenuto
     * per attuare dovute operazioni */
    while (EdgePanel.edgeAlive) {

      EdgeMessage messageToAnalize = bufferOfMessages.pull();

      String name = messageToAnalize.getSender();
      String ip = messageToAnalize.getSenderAddress();
      int port = messageToAnalize.getPortForEdges();

      /* Gestione messaggio di elezione */
      if (messageToAnalize.getContent() == EdgeMessage.Content.ELECTION) {

        /* Se sono appena entrato nella rete e mi arriva un messaggio di elezione prima del coordinatore
         * non partecipo all'elezione */
        if (EdgeWelcomeSensorsHandler.getInstance().isAlive()) {
          /* Ho indetto un elezione e mi arriva uno stesso
           * messaggio da un nodo con ID più piccolo, quindi gli rispondo con un OK */
          if (dataSensorsReader.isElectionInCourse()) {
            if (!node.getEdgesToNotifyOnElection().isEmpty()) {
              EdgeOkSender okSender = new EdgeOkSender(name, ip, port);
              okSender.setPriority(MAX_PRIORITY);
              okSender.start();
            }
          }
          /* In questo caso un nodo ha indetto un'elezione prima che me ne accorgessi */
          else dataSensorsReader.electionLauncher();
        }
        continue;
      }

      /* Messaggio del nuovo coordinatore, aggiorno il consumatore dei dati di sensore per impostargli ip e porta
       * del nuovo coordinatore a cui inviare i dati; infine chiudo l'elezione se era in corso */
      if (messageToAnalize.getContent() == EdgeMessage.Content.COORDINATOR) {

        /* Appena entro nella rete apro il thread per la lettura dei dati di sensore appena so chi è
         * il coordinatore */
        if (!EdgeWelcomeSensorsHandler.getInstance().isAlive())
          EdgeWelcomeSensorsHandler.getInstance().start();

        /* Che sia un'elezione o meno provvedo ad aggiornare le coordinate del nuovo coordinatore */
        node.setCoordinatorName(name);
        dataSensorsReader.refreshCoordinatorInfo(ip, port);

        if (dataSensorsReader.isElectionInCourse()) {
          System.out.println("Coordinator refreshed, now it's " + name);
        } else {
          System.out.println("Now I know " + name + ", the coordinator!");
        }
        dataSensorsReader.stopElection();
        continue;
      }

      /* Messaggio di ingresso del nodo edge che si e' connesso a me */
      if (messageToAnalize.getContent() == EdgeMessage.Content.HELLO) {

        /* Se e' in corso un'elezione attendo l'elezione del nuovo coordinatore il messaggio
         * prima di rispondere al nuovo nodo entrante */
        if (dataSensorsReader.isElectionInCourse()) {
          System.out.println(name + " blocked");
          new EdgeHelloWaitingRoom(name, ip, port, dataSensorsReader).start();
        } else new EdgeHelloRespondent(name, ip, port, dataSensorsReader).start();
        continue;
      }

      /* Messaggio di benvenuto da parte di un nodo edge a cui mi sono presentato */
      if (messageToAnalize.getContent() == EdgeMessage.Content.WELCOME) {
        EdgeInfo edgeRespondent = new EdgeInfo();
        edgeRespondent.setName(name);
        edgeRespondent.setIP(ip);
        edgeRespondent.setPortForEdgeNodes(port);

        /* Aggiunta nodo nella rete */
        node.addEdge(edgeRespondent);
        System.out.println("Now I know " + name);
        continue;
      }

      /* Messaggio di ok che ho ricevuto da un nodo con ID più grande, dopo l'invio di un messaggio d'elezione */
      if (messageToAnalize.getContent() == EdgeMessage.Content.OK) {

        if (dataSensorsReader.isElectionInCourse())
          System.out.println("Ok received from " + messageToAnalize.getSender());

        continue;
      }

      /* Ricevo una misurazione, può trattarsi di una statistica locale, che devo gestire opportunamente se sono
       * coordinatore, oppure può essere di una statistica globale che il coordinatore mi ha inviato come risposta */
      if (messageToAnalize.getContent() == EdgeMessage.Content.MEASUREMENT) {

        /* Spacchetto il messaggio per distinguere i due casi */
        Measurement measurementRetrived = messageToAnalize.getMeasurement();

        /* In caso positivo si tratta di una statistica globale di risposta a una locale che gli ho mandato
         * altrimenti significa che sono coordinatore */
        if (measurementRetrived.getType().startsWith("GlobalStat")) {
          EdgePanel.addGlobalStat(measurementRetrived);
          //      System.out.println("Last global stat: " + gson.toJson(measurementRetrived));
        } else {
          if (EdgePanel.coordinator && name.equals(node.getId()))
            EdgePanel.addLocalStat(measurementRetrived);
          /* Altrimenti sono il coordinatore, quindi ripondo con la più recente statistica globale */
          new EdgeMeasurementProducer(name, ip, port, messageToAnalize.getMeasurement()).start();
        }
      }
    }
  }
}
