package com.di.unimi.progetto.edgenode.coordinator;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.edgenode.EdgePanel;
import com.di.unimi.progetto.simulator.Measurement;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.reflect.TypeToken;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.List;
import javax.ws.rs.core.MediaType;

public class GlobalStatSender extends StatSender {

  private Gson gson;
  private Type edgesListType;

  public GlobalStatSender(String nodeId, Measurement measurement) {
    super(nodeId, measurement);
    gson = new GsonBuilder().create();
    edgesListType = new TypeToken<ArrayList<EdgeInfo>>() {}.getType();
  }

  @Override
  public void run() {
    /* Imposto chiamata rest al server cloud per la trasmissione della statistica globale */
    Client client = Client.create();

    WebResource cloudServerResource = client.resource("http://localhost:8888/cloud/");
    ClientResponse response =
        cloudServerResource
            .path("edge/global/" + name)
            .type(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, jsonCityStat);

    if (response.getStatus() == 200) {
      String jsonResponseEntity = response.getEntity(String.class);
      List<EdgeInfo> activeCoordinators = gson.fromJson(jsonResponseEntity, edgesListType);

      if (activeCoordinators.size() > 1) {
        EdgePanel.coordinator = false;
        CoordinatorConflictResolver resolver = new CoordinatorConflictResolver(activeCoordinators);
        resolver.start();
        try {
          resolver.join();
        } catch (InterruptedException e) {
          e.printStackTrace();
        }
      }
    }
  }
}
