package com.di.unimi.progetto.beans;

import java.util.Random;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class Position {

  private int x;
  private int y;

  public Position() {}

  public Position(int x, int y) {
    this.x = x;
    this.y = y;
  }

  public int getX() {
    return x;
  }

  public void setX(int x) {
    this.x = x;
  }

  public int getY() {
    return y;
  }

  public void setY(int y) {
    this.y = y;
  }

  public static int distance(Position p1, Position p2) {
    return Math.abs(p1.getX() - p2.getX()) + Math.abs(p1.getY() - p2.getY());
  }

  public static Position generateRandomPosition() {
    Random rnd = new Random();
    int x = rnd.nextInt(101);
    int y = rnd.nextInt(101);
    return new Position(x, y);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    Position position = (Position) o;

    if (getX() != position.getX()) return false;
    return getY() == position.getY();
  }

  @Override
  public int hashCode() {
    int result = getX();
    result = 31 * result + getY();
    return result;
  }
}
