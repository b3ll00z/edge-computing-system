package com.di.unimi.progetto.beans;

import java.util.*;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class City {

  private static City instance = null;

  private List<EdgeInfo> edges;
  private Map<String, Queue<CityStat>> globalStats;
  private Map<String, Queue<CityStat>> localStats;

  private City() {
    edges = new ArrayList<EdgeInfo>();
    globalStats = new HashMap<String, Queue<CityStat>>();
    localStats = new HashMap<String, Queue<CityStat>>();
  }

  // singleton City
  public static synchronized City getInstance() {

    if (instance == null) instance = new City();

    return instance;
  }

  /*********** INTERFACCIA CON NODI EDGE *********/

  public List<EdgeInfo> addEdge(EdgeInfo edgeInfo) {

    synchronized (edges) {

      // Controllo presenza del nodo
      if (isAlreadyPresent(edgeInfo.getName()))
        throw new IllegalArgumentException(edgeInfo.getName() + " is already in city");

      // Controllo se la posizione generata casualmente sia accettabile sulla base dello stato
      // attuale
      // della città
      if (!checkAvailablePositions(edgeInfo.getPositionInCity()))
        throw new IllegalArgumentException("Position refused, retry");

      /* Preparo struttura per accogliere le statistiche locali */
      prepareLocalStats(edgeInfo.getName());

      // non ci sono problemi, creo un edge info
      edges.add(edgeInfo);

      return new ArrayList<EdgeInfo>(edges);
    }
  }

  private void prepareGlobalStats(String coordinatorId) {

    synchronized (globalStats) {
      globalStats.put(coordinatorId, new PriorityQueue<CityStat>());
    }
  }

  private void prepareLocalStats(String edgeId) {

    synchronized (localStats) {
      localStats.put(edgeId, new PriorityQueue<CityStat>());
    }
  }

  public List<EdgeInfo> addGlobalStatistic(String coordinatorId, CityStat globalCityStat) {

    List<EdgeInfo> conflictingCoordinators = new ArrayList<>();

    /* Qualora si tratta di un coordinatore appena eletto rimuovo il suo mapping con le statistiche locali */
    synchronized (localStats) {
      if (localStats.containsKey(coordinatorId)) localStats.remove(coordinatorId);
    }

    synchronized (globalStats) {
      if (globalStats.size() > 1) {
        List<EdgeInfo> copyOfEdges = getCopyOfEdges();

        for (String coordinator : globalStats.keySet()) {
          for (EdgeInfo edge : copyOfEdges) {
            if (edge.getName().equals(coordinator)) {
              conflictingCoordinators.add(edge);
              break;
            }
          }
        }
        globalStats.clear();
        return conflictingCoordinators;
      }
      /* Preparazione struttura qualora fosse la prima volta che il coordinatore spedisca dati */
      if (!globalStats.containsKey(coordinatorId)) prepareGlobalStats(coordinatorId);

      Queue<CityStat> stats = globalStats.get(coordinatorId);
      stats.add(globalCityStat);
      globalStats.put(coordinatorId, stats);

      return conflictingCoordinators;
    }
  }

  public boolean addLocalStatistic(String edgeId, CityStat localCityStat) {

    synchronized (localStats) {
      try {
        Queue<CityStat> stats = localStats.get(edgeId);
        stats.add(localCityStat);
        localStats.put(edgeId, stats);
        return true;
      } catch (NullPointerException e) {
        removeEdgeNode(edgeId);
        System.out.println(edgeId + " dropped");
        return false;
      }
    }
  }

  public boolean removeEdgeNode(String edgeName) {

    EdgeInfo edgeToRemove = null;

    synchronized (edges) {
      for (EdgeInfo e : edges) {
        if (e.getName().equals(edgeName)) edgeToRemove = e;
      }

      if (edgeToRemove == null) return false;

      edges.remove(edgeToRemove);

      synchronized (globalStats) {
        if (globalStats.containsKey(edgeToRemove.getName())) {
          System.out.println(edgeToRemove.getName() + " coordinator dropped");
          globalStats.clear();
        }

        synchronized (localStats) {
          if (localStats.containsKey(edgeToRemove.getName())) {
            System.out.println(edgeToRemove.getName() + " dropped");
            String name = edgeToRemove.getName();
            localStats.remove(name);
          }
        }
      }
    }

    return true;
  }

  private boolean checkAvailablePositions(Position position) {

    // Controllo se si tratta del primo inserimento
    if (edges.size() == 0) return true;

    for (EdgeInfo node : getCopyOfEdges()) {
      if (Position.distance(node.getPositionInCity(), position) < 20) return false;
    }

    return true;
  }

  private boolean isAlreadyPresent(String edgeName) {

    synchronized (edges) {
      for (EdgeInfo edgeNode : edges) if (edgeNode.getName().equals(edgeName)) return true;

      return false;
    }
  }

  // ******* INTERFACCIA CON SENSORI ***********
  public EdgeInfo getNearestEdgeNode(Position sensorPosition) {

    List<EdgeInfo> copy = getCopyOfEdges();

    if (copy.isEmpty()) return null;

    EdgeInfo nearestEdge = copy.get(0);
    Position nearestPosition =
        copy.get(0).getPositionInCity(); // assumo che il primo nodo sia il piu vicino

    for (EdgeInfo node : copy.subList(1, copy.size())) {

      Position currentNodePosition = node.getPositionInCity();

      if (Position.distance(currentNodePosition, sensorPosition)
          < Position.distance(nearestPosition, sensorPosition)) {
        nearestEdge = node;
        nearestPosition = currentNodePosition;
      }
    }

    return nearestEdge;
  }

  // ******* INTERFACCIA CON ANALISTI *********

  // metodo per avere copia della griglia --> serve per letture
  public ArrayList<EdgeInfo> getCopyOfEdges() {

    synchronized (edges) {
      return new ArrayList<EdgeInfo>(edges);
    }
  }

  // metodo per avere copia delle statistiche globali
  private Map<String, Queue<CityStat>> getCopyOfGlobalStats() {

    synchronized (globalStats) {
      return new HashMap<String, Queue<CityStat>>(globalStats);
    }
  }

  // metodo per avere copia delle statistiche locali --> serve per letture
  private Map<String, Queue<CityStat>> getCopyOfLocalStats() {

    synchronized (localStats) {
      return new HashMap<String, Queue<CityStat>>(localStats);
    }
  }

  // Ultime n statistiche globali (con timestamp) della citta
  public Map<String, List<CityStat>> getLastGlobalStats(int how_many) {

    Map<String, Queue<CityStat>> copy = getCopyOfGlobalStats();
    Map<String, List<CityStat>> listCopy = new HashMap<>();

    for (String coordinator : copy.keySet()) {

      Queue<CityStat> globalStatsFromCoordinator = copy.get(coordinator);
      int size = globalStatsFromCoordinator.size();

      List<CityStat> listOfGlobalStats = new ArrayList<>(globalStatsFromCoordinator);
      try {
        listCopy.put(coordinator, listOfGlobalStats.subList(0, how_many));
      } catch (IndexOutOfBoundsException e) {
        listCopy.put(coordinator, listOfGlobalStats.subList(0, size));
      }
    }

    return listCopy;
  }

  // Ultime n statistiche (con timestamp) locali della città
  public Map<String, List<CityStat>> getLastLocalStats(int how_many) {

    Map<String, Queue<CityStat>> copy = getCopyOfLocalStats();
    Map<String, List<CityStat>> listCopy = new HashMap<>();

    for (String edgeName : copy.keySet()) {

      Queue<CityStat> localStatsFromEdge = copy.get(edgeName);
      int size = localStatsFromEdge.size();

      List<CityStat> listOfLocalStats = new ArrayList<>(localStatsFromEdge);
      try {
        listCopy.put(edgeName, listOfLocalStats.subList(0, how_many));
      } catch (IndexOutOfBoundsException e) {
        listCopy.put(edgeName, listOfLocalStats.subList(0, size));
      }
    }

    return listCopy;
  }

  // Ultime n statistiche (con timestamp) prodotte da uno specifico nodo
  public List<CityStat> getLastStatsFromEdgeNode(String edgeId, int how_many) {

    List<CityStat> lastStatsFromEdge;

    synchronized (localStats) {
      lastStatsFromEdge = new ArrayList<CityStat>(localStats.get(edgeId));
    }

    try {
      return lastStatsFromEdge.subList(0, how_many);
    } catch (IndexOutOfBoundsException e) {
      /* Se l'indice richiesto causa questa eccezione significa che how_many supera la dimensione
      della lista delle statistiche effettivamente raccolte, perciò gli do la lista completa. */
      return lastStatsFromEdge.subList(0, lastStatsFromEdge.size());
    }
  }

  // Deviazione standard e media delle ultime n statistiche prodotte da uno specifico nodo edge
  public Report computeLastStatsFromEdge(String edgeId, int size) {

    List<CityStat> lastStats = getLastStatsFromEdgeNode(edgeId, size);
    return getAverageAndStdDev(edgeId, lastStats, size);
  }

  // Deviazione standard e media delle ultime n statistiche globali della città
  public List<Report> computeLastGlobalStats(int size) {

    Map<String, List<CityStat>> lastGlobalStats = getLastGlobalStats(size);
    List<Report> result = new ArrayList<Report>();

    for (String coordinator : lastGlobalStats.keySet())
      result.add(getAverageAndStdDev(coordinator, lastGlobalStats.get(coordinator), size));

    return result;
  }

  private Report getAverageAndStdDev(String edgeId, List<CityStat> lastStats, int how_many) {

    double sumOfStats = 0, sumOfVariances = 0;
    double average;
    double stdDev;

    // Calcolo media
    for (CityStat stat : lastStats) sumOfStats += stat.getParticulateAverage();

    average = sumOfStats / how_many;

    // Calcolo deviazione std
    for (CityStat stat : lastStats) {
      double localAverage = stat.getParticulateAverage();
      sumOfVariances += ((localAverage - average) * (localAverage - average));
    }

    stdDev = Math.sqrt(sumOfVariances / how_many);
    return new Report(edgeId, average, stdDev);
  }
}
