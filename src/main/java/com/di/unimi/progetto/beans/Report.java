package com.di.unimi.progetto.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class Report {

  private String edgeNodeId;
  private double totalParticulateAverage;
  private double standardDeviation;

  public Report() {}

  public Report(String edgeNodeId, double totalParticulateAverage, double standardDeviation) {
    this.edgeNodeId = edgeNodeId;
    this.totalParticulateAverage = totalParticulateAverage;
    this.standardDeviation = standardDeviation;
  }

  public String getEdgeNodeId() {
    return edgeNodeId;
  }

  public void setEdgeNodeId(String edgeNodeId) {
    this.edgeNodeId = edgeNodeId;
  }

  public double getTotalParticulateAverage() {
    return totalParticulateAverage;
  }

  public void setTotalParticulateAverage(double totalParticulateAverage) {
    this.totalParticulateAverage = totalParticulateAverage;
  }

  public double getStandardDeviation() {
    return standardDeviation;
  }

  public void setStandardDeviation(double standardDeviation) {
    this.standardDeviation = standardDeviation;
  }
}
