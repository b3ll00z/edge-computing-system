package com.di.unimi.progetto.beans;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
@XmlAccessorType(XmlAccessType.FIELD)
public class EdgeInfo implements Comparable<EdgeInfo> {

  private String name;
  private String IP;
  private int portForSensors;
  private int portForEdgeNodes;
  private Position positionInCity;

  public EdgeInfo() {}

  public EdgeInfo(
      String name, String IP, int portForEdgeNodes, int portForSensors, Position positionInCity) {
    this.name = name;
    this.IP = IP;
    this.portForEdgeNodes = portForEdgeNodes;
    this.portForSensors = portForSensors;
    this.positionInCity = positionInCity;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String getIP() {
    return IP;
  }

  public void setIP(String IP) {
    this.IP = IP;
  }

  public int getPortForSensors() {
    return portForSensors;
  }

  public void setPortForSensors(int portForSensors) {
    this.portForSensors = portForSensors;
  }

  public int getPortForEdgeNodes() {
    return portForEdgeNodes;
  }

  public void setPortForEdgeNodes(int portForEdgeNodes) {
    this.portForEdgeNodes = portForEdgeNodes;
  }

  public Position getPositionInCity() {
    return positionInCity;
  }

  public void setPositionInCity(Position positionInCity) {
    this.positionInCity = positionInCity;
  }

  @Override
  public int compareTo(EdgeInfo o) {
    return name.compareTo(o.getName());
  }
}
