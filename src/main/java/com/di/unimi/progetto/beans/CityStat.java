package com.di.unimi.progetto.beans;

import javax.xml.bind.annotation.XmlRootElement;

@XmlRootElement
public class CityStat implements Comparable<CityStat> {

  private double particulateAverage;
  private long timestamp;

  public CityStat() {}

  public CityStat(double particulateAverage, long timestamp) {
    this.particulateAverage = particulateAverage;
    this.timestamp = timestamp;
  }

  public double getParticulateAverage() {
    return particulateAverage;
  }

  public void setParticulateAverage(double particulateAverage) {
    this.particulateAverage = particulateAverage;
  }

  public long getTimestamp() {
    return timestamp;
  }

  public void setTimestamp(long timestamp) {
    this.timestamp = timestamp;
  }

  @Override
  public int compareTo(CityStat o) {
    Long p = timestamp;
    Long q = o.getTimestamp();
    return p.compareTo(q);
  }
}
