package com.di.unimi.progetto.analist;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;

public class StartAnalist {

  private static String SERVER_CLOUD_URL = "http://localhost:8888/cloud/analist";
  private static Gson gson = new GsonBuilder().setPrettyPrinting().create();

  public static void main(String[] args) {

    BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    System.out.println("Welcome, please choose an item from menu");
    String answer;

    while (true) {
      showMenu();
      try {
        answer = br.readLine();

        if (answer.equals("1")) getCityState();
        else if (answer.equals("2")) getLastStatsFromEdgeNode();
        else if (answer.equals("3")) getLastGlobalAndLocalStats();
        else if (answer.equals("4")) getStdAndAverageFromEdgeNode();
        else if (answer.equals("5")) getStdAndAverageFromGlobalStats();
        else break;
      } catch (IOException e) {
        e.printStackTrace();
      }
    }
    System.out.println("Goodbye!");
  }

  private static void getCityState() {

    try {
      URL cloudServerURL = new URL(SERVER_CLOUD_URL);
      cloudServerSession(cloudServerURL);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void getLastStatsFromEdgeNode() {

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("Please specify an edge id to get report: ");
      String edgeId = br.readLine();

      System.out.print("How many statistics? ");
      int n = setAmountOfStats(br);
      URL cloudServerURL = new URL(SERVER_CLOUD_URL + "/" + edgeId + "/" + n);
      cloudServerSession(cloudServerURL);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void getLastGlobalAndLocalStats() {

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
      System.out.print("How many statistics? ");
      int n = setAmountOfStats(br);

      URL cloudServerURL = new URL(SERVER_CLOUD_URL + "/" + n);
      cloudServerSession(cloudServerURL);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void getStdAndAverageFromEdgeNode() {

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

      System.out.print("Please specify an edge id to get some stats: ");
      String edgeId = br.readLine();

      System.out.print("How many statistics? ");
      int n = setAmountOfStats(br);

      URL cloudServerURL = new URL(SERVER_CLOUD_URL + "/average/" + edgeId + "/" + n);
      cloudServerSession(cloudServerURL);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private static void getStdAndAverageFromGlobalStats() {

    try {
      BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

      System.out.print("How many statistics? ");
      int n = setAmountOfStats(br);

      URL cloudServerURL = new URL(SERVER_CLOUD_URL + "/average/" + n);
      cloudServerSession(cloudServerURL);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  private static int setAmountOfStats(BufferedReader br) throws IOException {
    int n;
    do {
      try {
        n = Integer.parseInt(br.readLine());
      } catch (NumberFormatException e) {
        n = 0;
        System.out.print("How many statistics? ");
      }
    } while (n == 0);
    return n;
  }

  private static void cloudServerSession(URL cloudServerURL) throws IOException {

    HttpURLConnection connection = (HttpURLConnection) cloudServerURL.openConnection();
    connection.setRequestMethod("GET");
    connection.setRequestProperty("Accept", "application/json");

    if (connection.getResponseCode() != 200) {
      System.out.println("Information not available");
      return;
    }

    BufferedReader br = new BufferedReader(new InputStreamReader((connection.getInputStream())));
    String jsonOutput;
    JsonParser jp = new JsonParser();

    while ((jsonOutput = br.readLine()) != null) {

      JsonElement je = jp.parse(jsonOutput);
      String prettyJsonOutput = gson.toJson(je);

      System.out.println(prettyJsonOutput);
    }
    connection.disconnect();
    System.out.println();
  }

  private static void showMenu() {
    System.out.println("1. See actual city state");
    System.out.println("2. Get last statistics from a specific edge node");
    System.out.println("3. Get last global and local city statistics");
    System.out.println(
        "4. Get standard derivation and particulate average from a specific edge node");
    System.out.println(
        "5. Get standard derivation and particulate average from last global statistics");
    System.out.print("--> ");
  }
}
