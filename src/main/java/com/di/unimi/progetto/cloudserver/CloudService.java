package com.di.unimi.progetto.cloudserver;

import com.di.unimi.progetto.beans.*;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.ws.rs.*;
import javax.ws.rs.core.Response;

@Path("cloud")
public class CloudService {

  // ****** SERVIZI PER CLIENT ANALISTI ********

  @Path("analist")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getTownState() {
    return Response.ok(City.getInstance().getCopyOfEdges()).build();
  }

  @Path("analist/{name}/{how_many}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getLastStatsFromEdge(
      @PathParam("name") String edgeNodeName, @PathParam("how_many") int n) {

    List<CityStat> lastEdgeStats;

    try {
      lastEdgeStats = City.getInstance().getLastStatsFromEdgeNode(edgeNodeName, n);
      return Response.ok(lastEdgeStats).build();
    } catch (NullPointerException e) {
      return Response.status(Response.Status.NOT_FOUND).build();
    }
  }

  @Path("analist/{how_many}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response getLastGlobalAndLocalStats(@PathParam("how_many") int n) {

    Map<String, List<CityStat>> lastGlobalStats = City.getInstance().getLastGlobalStats(n);
    Map<String, List<CityStat>> lastLocalStats = City.getInstance().getLastLocalStats(n);

    Map<String, List<CityStat>> lastStats = new HashMap<String, List<CityStat>>();
    lastStats.putAll(lastGlobalStats);
    lastStats.putAll(lastLocalStats);

    return Response.ok(lastStats).build();
  }

  @Path("analist/average/{name}/{how_many}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response computeAverageAndStdFromEdge(
      @PathParam("name") String edgeNodeName, @PathParam("how_many") int n) {

    Report result = City.getInstance().computeLastStatsFromEdge(edgeNodeName, n);

    if (result == null) return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    return Response.ok(result).build();
  }

  @Path("analist/average/{how_many}")
  @GET
  @Produces({"application/json", "application/xml"})
  public Response get(@PathParam("how_many") int n) {

    List<Report> result = City.getInstance().computeLastGlobalStats(n);

    if (result == null) return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();

    return Response.ok(result).build();
  }

  // ******* SERVIZI PER NODI EDGE *********
  @Path("edge")
  @POST
  @Consumes({"application/json", "application/xml"})
  @Produces({"application/json", "application/xml"})
  public Response addEdgeNode(EdgeInfo edgeInfo) {

    try {
      List<EdgeInfo> edgeNodesInCity = City.getInstance().addEdge(edgeInfo);
      return Response.ok(edgeNodesInCity).build();

    } catch (IllegalArgumentException e) {

      if (e.getMessage().equals(edgeInfo.getName() + " is already in city"))
        return Response.status(Response.Status.CONFLICT).build();

      if (e.getMessage().equals("Position refused, retry"))
        return Response.status(Response.Status.NOT_ACCEPTABLE).build();
    }

    return Response.status(Response.Status.INTERNAL_SERVER_ERROR).build();
  }

  @Path("edge/{name}")
  @DELETE
  public Response removeEdgeNode(@PathParam("name") String edgeNodeName) {

    if (City.getInstance().removeEdgeNode(edgeNodeName)) return Response.ok().build();

    return Response.status(Response.Status.NOT_FOUND).build();
  }

  @Path("edge/global/{name}")
  @POST
  @Consumes({"application/json", "application/xml"})
  @Produces({"application/json", "application/xml"})
  public Response addGlobalStat(@PathParam("name") String coordinatorName, CityStat stat) {

    try {
      List<EdgeInfo> coordinatorsName =
          City.getInstance().addGlobalStatistic(coordinatorName, stat);
      return Response.ok(coordinatorsName).build();
    } catch (Exception e) {
      return Response.status(Response.Status.CONFLICT).build();
    }
  }

  @Path("edge/local/{name}")
  @POST
  @Consumes({"application/json", "application/xml"})
  public Response addLocalStat(@PathParam("name") String edgeNodeName, CityStat stat) {

    if (City.getInstance().addLocalStatistic(edgeNodeName, stat)) return Response.ok().build();

    return Response.status(Response.Status.NOT_FOUND).build();
  }

  // ******* SERVIZI PER SENSORI ***********
  @Path("sensor")
  @POST
  @Consumes({"application/json", "application/xml"})
  @Produces({"application/json", "application/xml"})
  public Response getNearestEdgeNode(Position sensorPosition) {

    EdgeInfo response = City.getInstance().getNearestEdgeNode(sensorPosition);

    if (response == null) return Response.status(Response.Status.NO_CONTENT).build();

    return Response.ok(response).build();
  }
}
