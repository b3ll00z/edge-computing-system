package com.di.unimi.progetto.simulator;

import com.sun.jersey.api.client.ClientHandlerException;
import java.net.InetAddress;
import java.net.UnknownHostException;

public class StartSensorsSimulator {

  public static void main(String[] args) {

    if (args.length == 0) {
      System.out.print("You have to specify the address of server cloud ");
      System.out.println("and a number of sensors you may want to activate");
      System.exit(-1);
    }

    String serverHostAddress = args[0];
    InetAddress inetServerAddress;
    try {
      inetServerAddress = InetAddress.getByName(serverHostAddress);
    } catch (UnknownHostException e) {
      System.out.println("The address of server host is unknown");
      System.exit(-1);
    }

    int numberOfSensors = 0;

    try {
      numberOfSensors = Integer.parseInt(args[1]);
    } catch (NumberFormatException e) {
      System.out.println("You must specify a number, other stuff will be rejected");
      System.exit(-1);
    }

    try {
      for (int i = 0; i < numberOfSensors; i++) new StartSensorsHandler().start();
    } catch (ClientHandlerException e) {
      System.out.println("Server cloud not available");
      System.exit(-1);
    }
  }
}
