package com.di.unimi.progetto.simulator;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.net.SocketException;

public class SensorStreamImpl implements SensorStream {

  public static int ID = 1;

  private int id;
  private volatile Socket edgeSocket;
  private volatile DataOutputStream outToEdge;
  private Gson gson;

  public SensorStreamImpl() {
    id = ID++;
    gson = new GsonBuilder().create();
  }

  @Override
  public void sendMeasurement(Measurement m) {
    String jsonMeasurement = gson.toJson(m);
    try {
      outToEdge.writeBytes(jsonMeasurement + "\n");
      outToEdge.flush();
    } catch (SocketException e) {
      return;
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  public int getId() {
    return id;
  }

  public void setFirstEdgeSocket(String edgeAdress, int port) {
    try {
      edgeSocket = new Socket(edgeAdress, port);
      outToEdge = new DataOutputStream(edgeSocket.getOutputStream());
      System.out.println(id + ": First connection to " + edgeAdress + ":" + port);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }

  public void changeEdgeSocket(String edgeAddress, int port) {
    try {
      edgeSocket.close();
      edgeSocket = new Socket(edgeAddress, port);
      outToEdge = new DataOutputStream(edgeSocket.getOutputStream());
      System.out.println(id + ": Now I'm connected at " + edgeAddress + ":" + port);
    } catch (IOException e) {
      System.out.println(e.getMessage());
    }
  }
}
