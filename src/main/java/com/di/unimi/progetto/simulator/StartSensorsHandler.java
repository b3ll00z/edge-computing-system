package com.di.unimi.progetto.simulator;

import com.di.unimi.progetto.beans.EdgeInfo;
import com.di.unimi.progetto.beans.Position;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.sun.jersey.api.client.Client;
import com.sun.jersey.api.client.ClientResponse;
import com.sun.jersey.api.client.WebResource;
import javax.ws.rs.core.MediaType;

public class StartSensorsHandler extends Thread {

  protected volatile Position position;
  private PM10Simulator pm10Simulator;
  private SensorStreamImpl sensorStream;
  private volatile EdgeInfo nearestNode;
  private Gson gson;

  public StartSensorsHandler() {
    /* Finche' al primo avvio non trovo un nodo ripeto l'operazione */
    gson = new GsonBuilder().create();
    position = Position.generateRandomPosition();
    nearestNode = findNearestNode();
    sensorStream = new SensorStreamImpl();
    pm10Simulator = new PM10Simulator(sensorStream);
  }

  public void run() {

    int sensorId = sensorStream.getId();
    try {
      /* Collego via socket lo stream verso il nuovo nodo più vicino e avvio il simulatore*/
      sensorStream.setFirstEdgeSocket(nearestNode.getIP(), nearestNode.getPortForSensors());
      pm10Simulator.start();
    } catch (NullPointerException e) {
      System.out.println(sensorId + ": There's no edge in the city at the moment");
      return;
    }
    Position oldPosition = nearestNode.getPositionInCity();

    /* Ogni dieci secondi domando al server il nodo più vicino, che verrà accettato se diverso da quello precedente
     * sulla base di IP e PORTA
     */
    while (true) {
      try {
        sleep(10000);
        EdgeInfo newEdge = findNearestNode();

        if (!oldPosition.equals(newEdge.getPositionInCity())) {
          nearestNode = newEdge;
          oldPosition = nearestNode.getPositionInCity();
          sensorStream.changeEdgeSocket(nearestNode.getIP(), nearestNode.getPortForSensors());
          System.out.println(
              sensorId
                  + ": Found new nearest node at "
                  + newEdge.getIP()
                  + ":"
                  + newEdge.getPortForSensors());
        } else System.out.println(sensorId + ": Nearest node remains the same");
      } catch (NullPointerException e) {
        System.out.println("Edge not found! Let's retry with other position");
        position = Position.generateRandomPosition();
      } catch (InterruptedException e) {
        e.printStackTrace();
        break;
      }
    }
  }

  private EdgeInfo findNearestNode() {
    /* Predispozione chiamata REST al server cloud per cercare un nuovo edge con posizione più vicina */
    Client client = Client.create();
    WebResource cloudServerResource = client.resource("http://localhost:8888/cloud/sensor");

    String jsonAttempedPosition = gson.toJson(position);

    ClientResponse response =
        cloudServerResource
            .type(MediaType.APPLICATION_JSON)
            .accept(MediaType.APPLICATION_JSON)
            .post(ClientResponse.class, jsonAttempedPosition);

    if (response.getStatus() == 200) {
      String jsonResponseEntity = response.getEntity(String.class);
      return gson.fromJson(jsonResponseEntity, EdgeInfo.class);
    }

    return null;
  }
}
