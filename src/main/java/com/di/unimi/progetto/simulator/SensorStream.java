package com.di.unimi.progetto.simulator;

public interface SensorStream {

  void sendMeasurement(Measurement m);
}
